/*
Iconzz mod for Wurm Unlimited
Copyright (C) 2024 Tyoda

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
package org.tyoda.wurm.Iconzz;

import com.wurmonline.server.Server;
import com.wurmonline.server.items.Item;
import com.wurmonline.server.players.Player;
import org.gotti.wurmunlimited.modloader.ReflectionUtil;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

/**
 * Handles refreshing the icons in character windows
 */
public class RefreshCharacterWindow {
    private static class RenameClass implements Runnable {
        private final Player player;
        RenameClass(Player player){
            this.player = player;
        }

        @Override
        public void run() {
            Iconzz.logger.info("Refreshing character window icons for player: "+player.getName());
            for (Item equip : player.getBody().getAllItems()) {
                String name = equip.getActualName();
                equip.setName(equip.getName() + ' ', true);
                equip.setName(name, true);
            }
        }
    }

    public void onPlayerLogin(Player player){
        try {
            ((ScheduledExecutorService)ReflectionUtil.getPrivateField(Server.getInstance(), ReflectionUtil.getField(Server.class, "scheduledExecutorService")))
                    .schedule(new RenameClass(player), Iconzz.getInstance().getSecondsBeforeUpdate(), TimeUnit.SECONDS);
        } catch (NoSuchFieldException | IllegalAccessException e){
            Iconzz.logger.log(Level.SEVERE, "Callable interrupted.", e);
        }
    }
}
