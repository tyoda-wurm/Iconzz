# Iconzz
A small mod API allowing the easy use of custom icons in the game.

You need THE LATEST (version 0.45+) ago's server modlauncher for this to work.

NOTE: This mod will not be compatible with any other mods that use icons without using this library. However, the worst that should happen is either this or the other mod's icons will show up as weird, random icons. If you find such a mod, please report it here and we'll figure something out. Currently Coldie's windmill mod and ~~[Arathok's Alchemy mod](https://github.com/Arathok/Wurm-Unlimited-Alchemy) (version 0.8.9) is known not to be compatible.~~. **Arathok's alchemy mod is now compatible.**
# Features
Add a new icon to the game by simply adding this mod as a library in your .properties file:

`depend.requires=Iconzz`

And adding one line in the code:

`short iconId = Iconzz.getInstance().addIcon("IconName", "mods/ExampleMod/icons/IconName.png");`

You can then use this ID while creating item templates to use the newly added icon:

`ItemTemplateBuilder exampleItem = new ItemTemplateBuilder("ExampleItem").imageNumber(iconId);`

For an example, see the <a href="https://gitlab.com/tyoda-wurm/IconzzExample">Example mod</a>, or watch this part of my [modding tutorial](https://youtu.be/P8fNV9R1LS8?t=2220)

NOTE: The icon images must be 32x32, and the IconName in addIcon must be unique among all mods that use this library.

### Additional improvements to the icons in the game:
 - Huge Bell will now have an icon
 - Golden Mirror will now have an icon
 - Removed weird random lines on the borders of some icons
 - Removed default icon cube from behind the muffin icon lol
 - You can edit the images in the mod's resources folder to customize the game's icons to your liking!

# Add to gradle

To include Iconzz in your gradle project, add it to the repositories in build.gradle

```gradle
repositories {
    // other repositories
    maven { url "https://gitlab.com/api/v4/groups/83701471/-/packages/maven" }
}
```

and then include it as a dependency as

```gradle
dependencies {
    // other dependencies
    compileOnly 'org.tyoda.wurm.Iconzz:Iconzz:<VERSION>'
}
```

or

```gradle
dependencies {
    // other dependencies
    implementation 'org.tyoda.wurm.Iconzz:Iconzz:<VERSION>'
}
```

replacing `<VERSION>` with the current latest release version.

# Add to maven

To include Iconzz in your maven project, add it to the repositories in pom.xml


```xml
<repository>
    <id>iconzz-gitlab</id>
    <name>Iconzz GitLab repository</name>
    <url>https://gitlab.com/api/v4/groups/83701471/-/packages/maven</url>
</repository>
```

Then add the depndency

```xml
<dependency>
  <groupId>org.tyoda.wurm.Iconzz</groupId>
  <artifactId>Iconzz</artifactId>
  <version><VERSION></version>
</dependency>
```

replacing `<VERSION>` with the current latest release version.

(full disclosure: I don't know anything about maven, hopefully nobody tries to use this)

# Credits
 - Many thanks to bdew for figuring out how to use serverpacks functions in the sharedClassLoader context, as seen in this [TestMod](https://gist.github.com/bdew/5ee9fcb39da73405a38810116104e101)
